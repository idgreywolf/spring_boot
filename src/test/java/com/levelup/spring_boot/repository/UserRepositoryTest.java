package com.levelup.spring_boot.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@MockBean (DataSource.class)
@TestPropertySource("classpath:application.properties")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void repositoryExist() {
        assertNotNull(userRepository);
    }

    @Test
    public void save() throws Exception {
    }

}