package com.levelup.spring_boot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UserEntity {

    private int id;
    private String login;
    private String password;

}
