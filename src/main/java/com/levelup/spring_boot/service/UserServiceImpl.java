package com.levelup.spring_boot.service;

import com.levelup.spring_boot.dto.UserDTO;
import com.levelup.spring_boot.repository.UserRepository;
import com.levelup.spring_boot.service.intrf.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public void save(UserDTO user) throws SQLException {
        userRepository.save(user.getLogin(), user.getPassword());
    }

    @Override
    public List<UserDTO> getAll() throws SQLException {
        return userRepository.getAll().stream()
                .map(user -> new UserDTO().setLogin(user.getLogin()).setPassword(user.getPassword()))
                .collect(Collectors.toList());


    }
}
