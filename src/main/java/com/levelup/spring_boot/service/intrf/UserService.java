package com.levelup.spring_boot.service.intrf;

import com.levelup.spring_boot.dto.UserDTO;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    void save(UserDTO userDTO) throws SQLException;
    List<UserDTO> getAll() throws SQLException;
}
