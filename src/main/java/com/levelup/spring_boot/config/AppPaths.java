package com.levelup.spring_boot.config;

public class AppPaths {

    public static final String ROOT = "/";

    public static final String USER = "/user";

    public static final String USER_LIST = USER + "/list";

    public static final String FORM = "/form";

    public static final String ABOUT = "/about";

    public static final String JS_EXAMPLE = "/js-example";

    public static final String JQUERY_EXAMPLE = "/jquery-example";
}
