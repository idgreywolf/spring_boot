package com.levelup.spring_boot.controller;

import com.levelup.spring_boot.dto.UserDTO;
import com.levelup.spring_boot.service.intrf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.List;

import static com.levelup.spring_boot.config.AppPaths.USER_LIST;

@Controller
public class ListExampleController {

@Autowired
  UserService userService;

    @RequestMapping(USER_LIST)
    public String getList(Model model) throws SQLException {
        List<UserDTO> all = userService.getAll();
        model.addAttribute("listExample", all);
        return "listExample";
    }

}
