package com.levelup.spring_boot.controller.rest;

import com.levelup.spring_boot.dto.UserDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FakeRestController {


    @PostMapping("/fake-rest")
    public UserDTO get(@RequestBody UserDTO user) {
        return user;
    }
}
