package com.levelup.spring_boot.controller.rest;

import lombok.Data;

@Data
public class ParamDTO {
    private String param;
}
