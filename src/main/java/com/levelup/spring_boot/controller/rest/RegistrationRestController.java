package com.levelup.spring_boot.controller.rest;


import com.levelup.spring_boot.dto.UserDTO;
import com.levelup.spring_boot.security.AppAuthProvider;
import com.levelup.spring_boot.security.AppUserDetails;
import com.levelup.spring_boot.service.intrf.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationRestController {

    private final AppAuthProvider authProvider;

    private final UserService userService;

    @PostMapping("/registration")
    public void registration(UserDTO userDTO, HttpServletResponse response) throws SQLException, IOException {
        userService.save(userDTO);
        Authentication authenticate = authProvider.authenticate(new UsernamePasswordAuthenticationToken(userDTO.getLogin(), userDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        response.sendRedirect("/");
    }
}
