package com.levelup.spring_boot.controller.rest;


import com.levelup.spring_boot.config.AppPaths;
import com.levelup.spring_boot.dto.UserDTO;
import com.levelup.spring_boot.service.intrf.UserService;
import com.levelup.spring_boot.validator.UserDTOValidator;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@AllArgsConstructor (onConstructor = @__(@Autowired))
public class UserFormController {

    private final UserDTOValidator userDTOValidator;

    private final UserService userService;

    @InitBinder
    public void userDTOBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(userDTOValidator);
    }

    @RequestMapping(value = AppPaths.FORM, method = POST)
    public String save(@Validated UserDTO user, BindingResult bindingResult, Model model) throws SQLException {
        if (bindingResult.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            bindingResult.getAllErrors().forEach(error -> errors.put(((FieldError)error).getField(), error.getDefaultMessage()));
            model.addAttribute("errors", errors);
            model.addAttribute("user", user);
            return "form";
        }
        userService.save(user);
        return "listExample";
    }

}
