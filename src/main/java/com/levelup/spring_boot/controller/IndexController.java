package com.levelup.spring_boot.controller;

import com.levelup.spring_boot.config.AppPaths;
import com.levelup.spring_boot.security.AppUserDetails;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {


    @RequestMapping(AppPaths.ROOT)
    public String getRoot(Model model, @AuthenticationPrincipal AppUserDetails appUserDetails) {
        model.addAttribute("nickname", appUserDetails.getNickName());
        return "index";
    }

    @RequestMapping(AppPaths.ABOUT)
    public String getAbout() {
        return "about";
    }


}
