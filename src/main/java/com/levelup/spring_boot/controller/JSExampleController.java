package com.levelup.spring_boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static com.levelup.spring_boot.config.AppPaths.JS_EXAMPLE;

@Controller
public class JSExampleController {

    @GetMapping(JS_EXAMPLE)
    public String get() {
        return "jsExample";
    }
}
