package com.levelup.spring_boot.controller;

import com.levelup.spring_boot.config.AppPaths;
import com.levelup.spring_boot.dto.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormController {

    @RequestMapping(AppPaths.FORM)
    public String get(Model model) {
        model.addAttribute("user", new UserDTO());
        return "form";
    }
}
