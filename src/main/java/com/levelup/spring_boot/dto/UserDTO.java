package com.levelup.spring_boot.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Accessors(chain = true)
public class UserDTO {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;
}
