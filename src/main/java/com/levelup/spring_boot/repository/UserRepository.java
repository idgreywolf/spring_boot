package com.levelup.spring_boot.repository;

import com.google.common.collect.Lists;
import com.levelup.spring_boot.model.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserRepository {


    private final DataSource dataSource;

    public List<UserEntity> getAll() throws SQLException {
       List<UserEntity> userEntities = Lists.newArrayList();
        PreparedStatement preparedStatement = dataSource.getConnection().prepareStatement("SELECT * FROM user");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){

           String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            userEntities.add( new UserEntity().setLogin(login).setPassword(password));


        }
        return userEntities;
    }

    public void save(String login, String password) throws SQLException {

        PreparedStatement preparedStatement =
                dataSource.getConnection().prepareStatement("INSERT INTO users VALUES(? ,?, ?, ?)");

        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        preparedStatement.setString(3, "ROLE_USER");
        preparedStatement.setString(4, login);

        preparedStatement.execute();
    }

}
