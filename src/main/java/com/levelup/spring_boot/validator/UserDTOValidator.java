package com.levelup.spring_boot.validator;

import com.levelup.spring_boot.dto.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static org.springframework.util.StringUtils.isEmpty;

@Component
public class UserDTOValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return UserDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTO user = (UserDTO) target;
        if (isEmpty(user.getLogin())) {
            errors.rejectValue("login", "400", "Login should not be empty!");
        }
        if (isEmpty(user.getPassword())) {
            errors.rejectValue("password", "400", "Password should not be empty!");
        }
    }
}
