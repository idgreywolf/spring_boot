package com.levelup.spring_boot.security;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AppUserDetailsService implements UserDetailsService {

    private final DataSource dataSource;

    @Override
    public UserDetails loadUserByUsername(String username) {
        try {
            log.info("Trying to login {} {}", username);
            PreparedStatement findUser = dataSource.getConnection().prepareStatement("SELECT * FROM users WHERE username = ?");
            findUser.setString(1, username);
            ResultSet resultSet = findUser.executeQuery();
            if (resultSet.next()) {
                String login = resultSet.getString("username");
                String password = resultSet.getString("password");
                String role = resultSet.getString("role");
                String nickName = resultSet.getString("nickname");
                log.info("Login successful {}", username);
                return new AppUserDetails(login, password, nickName, role);
            } else {
                log.error("Login error {}", username);
                throw new UsernameNotFoundException("User not found");
            }
        } catch (SQLException e) {
            log.error("Database error!", e);
            throw new UsernameNotFoundException("Database error");
        }
    }
}
