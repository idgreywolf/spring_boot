package com.levelup.spring_boot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class AppAuthProvider extends DaoAuthenticationProvider {

    @Autowired
    public AppAuthProvider(UserDetailsService userDetailsService) {
        setUserDetailsService(userDetailsService);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        if ("Iluha".equals(name)) {
            throw new BadCredentialsException("Iluha ne znaet kak bezopasno proverjat string");
        } else {
            return super.authenticate(authentication);
        }
    }
}
