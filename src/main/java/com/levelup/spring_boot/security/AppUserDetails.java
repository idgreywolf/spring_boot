package com.levelup.spring_boot.security;

import com.google.common.collect.Lists;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

@Getter
public class AppUserDetails extends User{

    private final String nickName;

    public AppUserDetails(String username, String password, String nickName, String role) {
        super(username, password, Lists.newArrayList(new SimpleGrantedAuthority(role)));
        this.nickName = nickName;
    }

}
